/**
 * @file добавляет методы для плагина jQuery Validation
 * Данный скрипт должен быть подключен строго ПОСЛЕ jQuery Validation
 * и ДО основного скрипта
 */

/**
 * Валидация на неверный формат даты:
 * срок действия должен быть в формате MM/YY,
 * где MM - от 01 до 12
 */
jQuery.validator.addMethod(
  "expirationdate",
  function (value, element) {
    return (
      this.optional(element) || /^(0?[1-9]|1[012])\/([0-9]{2})$/.test(value)
    );
  },
  "Неверный формат даты"
);

/**
 * Валидация на истёкший срок действия:
 * срок действия должен быть не позднее текущего месяца
 */
jQuery.validator.addMethod(
  "notexpired",
  function (value, element) {
    // Формат значения поля считается валидным (MM/YY)
    const [month, year] = value.split("/");

    return (
      this.optional(element) || new Date(+year + 2000, +month, 1) >= new Date()
    );
  },
  "Срок действия карты истёк"
);
