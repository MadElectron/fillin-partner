/**
 * @file главный скрипт формы
 * Для обработки событий формы используется jQuery
 */

// ===== Переменные и константы =====

/**
 * Ключ для API FillIn
 */
const FILLIN_API_KEY = "1234567890abcdef";

/**
 * URL для платёжного запроса к API FillIn
 */
const FILLIN_API_URL = "https://api.apps.rktech.cloud/payment";

/**
 * Партнёрский callback URL
 */
const CALLBACK_URL = "https://example.com";

/**
 * Правила валидации полей:
 * используются плагином jQuery Validation
 */
const rules = {
  summ: {
    required: true,
    range: [100, 5000],
  },
  number: {
    required: true,
    creditcard: true,
  },
  valid_thru: {
    expirationdate: true,
    notexpired: true,
  },
  cvv: {
    required: true,
  },
};

/**
 * Сообщения об ошибках для невалидных полей:
 * используются плагином jQuery Validation
 */
const messages = {
  summ: {
    required: "Обязательное поле",
    range: "Некорректная сумма",
  },
  number: {
    required: "Обязательное поле",
    creditcard: "Неверный формат карты",
  },
  valid_thru: {
    required: "Обязательное поле",
  },
  cvv: {
    required: "Обязательное поле",
  },
};

// ===== Функции =====

/**
 * Добавляет маски ввода для полей формы:
 * использует метод 'mask' плагина jQuery Mask
 * @see {@link https://igorescobar.github.io/jQuery-Mask-Plugin/docs.html#basic-usage}
 */
const initInputMasks = () => {
  $("#number").mask("0000 0000 0000 0000");
  $("#valid_thru").mask("00/00");
};

/**
 * Сброс формы в исходное состояние
 */
const resetForm = function () {
  // Сброс всех полей формы
  $("#form").trigger("reset");

  // Реинициализация масок ввода для полей формы
  initInputMasks();

  // Закрывается модальное окно ("Пожалуйста, не обновляйте страницу.")
  $("#waitModal").hide();
};

/**
 * Показ модального окна ошибки
 * @param {string} message - Текст сообщения об ошибке
 */
const triggerError = function (message) {
  // Вставка сообщения в нужный элемент
  $("#errorModalMessage").text(message);

  // Показ модального окна
  $("#errorModal").modal("show");
};

/**
 * Конвертация FormData в JSON
 * @param {Array} arr - исходная коллекция FormData в формате [{ name: name1, value: value1 }, {...}]
 * @returns {Object} результирующий JSON-объект в формате { [name1]: value1, [name2]: value2 }
 * @example
 * [{ foo: 'bar'}, { baz: 100 }] => { foo: 'bar', baz: 100 }
 */
const convertFormDataToJson = (arr) => {
  // Пары ключ-значение для результирующего объекта в формате [[name1, value1], [name2, value2]]
  const pairs = arr.map((el) => [el.name, el.value]);

  // Результирующий обхект, собранный из пар ключ-значение
  const json = _.fromPairs(pairs);

  return json;
};

// ===== События =====

/**
 * При измении любого поля, проверить, валидна ли форма
 * для активации кнопки "Отправить"
 */
$("#form").on("keyup change paste", "input, select, textarea", function () {
  // Сначала поверяется, заполнены ли все поля
  const filledInputs = $("#form input[required]")
    .map(function () {
      return !!$(this).val();
    })
    .toArray(); // Статусы заполнения полей формы в виде массива булевых значений
  const isFormFilled = filledInputs.every((el) => el);

  // Затем проверяется, валидны ли поля
  let isFormValid = false;

  if (isFormFilled) {
    /**
     * Функция валидации применяется, только если форма заполнена,
     * чтобы не отображать ошибки для пустых полей
     * @see {@link https://jqueryvalidation.org/valid/ }
     */
    isFormValid = $("#form").valid();
  }

  // Если форма заполнена и валидна, кнопка "Отправить" активируется
  if (isFormFilled && isFormValid) {
    $("#submit").removeAttr("disabled");
  } else {
    $("#submit").attr("disabled", "disabled");
  }
});

/**
 * При изменении значения любого  поля форма валидируется
 * @see {@link https://jqueryvalidation.org/validate/}
 */
$("input").change(function () {
  $("#form").validate({
    rules,
    messages,
    success: false, // При успешной валидации ничего не происходит
  });
});

/**
 * При подтверждении формы, данные отправлятся на сервер
 * и открывается модальное окно "Пожалуйста, не обновляйте страницу."
 */
$("#form").on("submit", function (e) {
  e.preventDefault();

  // Сначала показывается модальное окно
  $("#waitModal").show();

  /**
   * С полей снимаются маски ввода, чтобы в FormData оказались верные данные
   * (номер карты без пробелов и срок действия без слэша)
   * @see {@link https://igorescobar.github.io/jQuery-Mask-Plugin/docs.html#removing-the-mask}
   */
  $("input.masked").unmask();

  // Формаирование данных для отправкиЖ
  const formData = $(this).serializeArray(); // Данные формы в виде коллекции
  let json = convertFormDataToJson(formData); // Сконвертированная в объект formData

  // К данным добавляется партнёрский Callback URL
  json.callback_url = CALLBACK_URL;

  // Сумма должна быть сконвертирована в целочисленное значение
  json.summ = parseInt(json.summ);

  const data = JSON.stringify(json); // Данные для отравки

  /**
   * Отправка данных:
   * Данные отправляются методом POST в формате JSON
   * с токеном авторизации
   */
  $.ajax(FILLIN_API_URL, {
    method: "POST",
    contentType: "application/json",
    headers: {
      Authorization: FILLIN_API_KEY,
    },
    data,
  })
    .done(function (resp) {
      /** Получив стандартный код 301 при нормальном отклике
       * партнер ДОЛЖЕН обеспечить редирект пользователя на redirect_url ссылку
       * для того чтобы пользователь мог пройти банковскую 3DSecure процедуру.
       */
      location.replace(resp.redirect_url);
      // Скрипт здесь завершается, так как пользователь переходит на страницу банка
    })
    .fail(function (err) {
      /**
       * При неудачном запросе форма сбрасывается,
       * и пользователю показывается модальное окно с ошибкой
       */
      console.error(err);
      resetForm();
      triggerError(err.error_message || "Неизвестная ошибка");
    });
});

/**
 * При загрузке страницы происходит инициализация масок ввода для полей формы
 */
$(document).ready(function () {
  initInputMasks();
});
