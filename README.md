# Fillin Partner

Пример партнёрской формы для отправки данных на API веб-сервиса FillIn.
HTML-страница использует [Bootstrap 5.1](https://getbootstrap.com/docs/5.1/getting-started/introduction/).
Скрипты используют [jQuery 3.6](https://api.jquery.com/) с плагинами [jQuery Mask](https://igorescobar.github.io/jQuery-Mask-Plugin/) и [jQuery Validation](https://jqueryvalidation.org/).
